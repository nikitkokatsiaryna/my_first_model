python -m venv env
source env/bin/activate
pip install -r ~/odoo/requirements.txt
pip install PyPDF2
pip install Werkzeug
pip install python-dateutil
pip install reportlab
