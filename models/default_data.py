# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Checkbox(models.Model):
    _inherit = 'res.partner'

    @api.model
    def default_get(self, fields_list):
        res = super(Checkbox, self).default_get(fields_list)
        res['phone'] = '1'
        res['mobile'] = '2'
        res['email'] = '3'
        return res
