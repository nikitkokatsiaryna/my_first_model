# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Contacts(models.Model):
    _inherit = "res.partner"

    private_num = fields.Char()
    passport_num = fields.Char()
    data_of_BD = fields.Date()
    passport_authority = fields.Text()
    date_of_issue = fields.Date()
