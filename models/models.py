# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Teachers(models.Model):
    _name = 'academy.teachers'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mail.activity.mixin']

    name = fields.Char()
    biography = fields.Html()
    photo = fields.Binary()
    age = fields.Integer()
    level = fields.Float()
    patient = fields.Boolean()
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
    ], help='Gender'
    )
    staff_dob = fields.Date()
    datetime = fields.Datetime()
    description = fields.Text()
    # salary = fields.Monetary()

    course_ids = fields.One2many('product.template', 'teacher_id', string="Courses")

    # related fields
    # связанный филд
    # syntax
    # field_name = fields.Char(related=many_2_one_field.field_name_from_this_model)


class Courses(models.Model):
    _inherit = 'product.template'

    name = fields.Char()
    teacher_id = fields.Many2one('academy.teachers', string="Teacher", required=False, readonly=False, )
    books_ids = fields.Many2many('book.template', string="Books")


class Book(models.Model):
    _name = 'book.template'

    name = fields.Char()


class MainAttr(models.Model):
    _name = 'algorithm.template'

    name = fields.Char()

    first_field = fields.Selection([
        ('one', '1'),
        ('two', '2'),
        ('three', '3')
    ])

    second_field = fields.Selection([
        ('four', '4'),
        ('five', '5'),
        ('six', '6')
    ])

    field_1 = fields.Text(string='Field 1')

    field_2 = fields.Text(string='Field 2')

    field_3 = fields.Text(string='Field 3')

    field_4 = fields.Text(string='Field 4')

    field_5 = fields.Text(string='Field 5')

    field_6 = fields.Text(string='Field 6')

    field_7 = fields.Text(string='Field 7')

    field_8 = fields.Text(string='Field 8')

    field_9 = fields.Text(string='Field 9')


class Checkbox(models.Model):
    _name = 'checkbox.template'

    name = fields.Char()

    first_field = fields.Selection([
        ('one', '1'),
        ('two', '2'),
        ('three', '3')
    ])

    second_field = fields.Selection([
        ('four', '4'),
        ('five', '5'),
        ('six', '6')
    ])

    a = fields.Boolean()

    b = fields.Boolean()

    c = fields.Boolean()

    def all_onchange(self):
        if not self.b and not self.c:
            self.name = None
        elif not self.b and self.c:
            self.name = f"Name {dict(self._fields['second_field'].selection).get(self.second_field)}"
        elif not self.c and self.b:
            self.name = f"Name {dict(self._fields['first_field'].selection).get(self.first_field)}"
        elif self.b and self.c:
            self.name = f"Name {dict(self._fields['first_field'].selection).get(self.first_field)} Name {dict(self._fields['second_field'].selection).get(self.second_field)}"
        else:
            self.name = None

    @api.onchange('b')
    def onchange_field_b(self):
        self.all_onchange()

    @api.onchange('c')
    def onchange_field_c(self):
        self.all_onchange()

    @api.onchange('a')
    def onchange_field_a(self):
        if self.a:
            self.b, self.c = True, True
            self.all_onchange()
        else:
            self.b, self.c = False, False
            self.all_onchange()

