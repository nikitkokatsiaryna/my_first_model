# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Checkbox(models.Model):
    _name = 'checkbox.template'

    name = fields.Char()
    # email = fields.Char()

    a = fields.Boolean()

    @api.multi
    def create_contact(self):
        view_id = self.env.ref('base.view_partner_form').id
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'res.partner',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'context': {
                'default_name': self.name,
            }
        }

    @api.multi
    def create_contact_company(self):
        view_id = self.env.ref('base.view_partner_form').id
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'res.partner',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'context': {
                'default_name': self.name,
                'default_company_type': 'company',
                "default_is_company": True,
            }
        }
