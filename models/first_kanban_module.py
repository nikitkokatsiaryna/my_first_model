# -*- coding: utf-8 -*-

from odoo import models, fields, api

AVAILABLE_PRIORITIES = [
    ('0', 'Low'),
    ('1', 'Medium'),
    ('2', 'High'),
    ('3', 'Very High'),
]


class Partner(models.Model):
    _name = 'person'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'utm.mixin', 'format.address.mixin', 'mail.blacklist.mixin']

    name = fields.Char('Name', required=True, index=True)
    last_name = fields.Char()
    birthday = fields.Date()
    contact = fields.Char()
    email = fields.Char()
    city = fields.Char()
    phone = fields.Char()
    tag_ids = fields.Many2many('crm.lead.tag', 'crm_lead_tag_rel', 'lead_id', 'tag_id', string='Tags',
                               help="Classify and analyze your lead/opportunity categories like: Training, Service")
    priority = fields.Selection(AVAILABLE_PRIORITIES, string='Priority', index=True, default=AVAILABLE_PRIORITIES[0][0])
    partner_id = fields.Many2one('res.partner', string='Partner', track_visibility='onchange', track_sequence=1,
                                 index=True)
    # stage_id = fields.Many2one('crm.stage', string='Stage', ondelete='restrict', track_visibility='onchange',
    #                            index=True,
    #                            domain="['|', ('team_id', '=', False), ('team_id', '=', team_id)]",
    #                            # group_expand='_read_group_stage_ids', default=lambda self: self._default_stage_id())
    #                            group_expand='_read_group_stage_ids')
    # user_id = fields.Many2one('res.users', string='Salesperson', track_visibility='onchange', default=lambda self: self.env.user)
    # active = fields.Boolean('Active', default=True, track_visibility=True)
    # team_id = fields.Many2one('crm.team', string='Sales Team', oldname='section_id',
    #                           default=lambda self: self.env['crm.team'].sudo()._get_default_team_id(
    #                               user_id=self.env.uid),
    #                           index=True, track_visibility='onchange',
    #                           help='When sending mails, the default email address is taken from the Sales Team.')
    stage = fields.Selection([
        ('concept', 'Concept'),
        ('started', 'Started'),
        ('progress', 'In progress'),
        ('finished', 'Done'),
    ], default='concept')


    partner_name = fields.Char()
    street = fields.Char()
    street2 = fields.Char()
    state_id = fields.Integer()
    country_id = fields.Integer()
    website = fields.Char()
    contact_name = fields.Char()
    title = fields.Char()
    function = fields.Char()
    mobile = fields.Integer()

    description = fields.Text()
    checkbox_all = fields.Boolean()
    company_id = fields.Integer()
    campaign_id = fields.Integer()
    medium_id = fields.Integer()
    source_id = fields.Integer()
    referred = fields.Char()
    day_open = fields.Date()
    day_close = fields.Date()

    checkbox1 = fields.Boolean()
    checkbox2 = fields.Boolean()
    gender = fields.Selection((('male', 'Male'), ('female', 'Female'), ('other', 'Other')), help='Gender')

    @api.one
    def concept_progressbar(self):
        self.write({
            'stage': 'concept',
        })

    # This function is triggered when the user clicks on the button 'Set to started'
    @api.one
    def started_progressbar(self):
        self.write({
            'stage': 'started'
        })

    # This function is triggered when the user clicks on the button 'In progress'
    @api.one
    def progress_progressbar(self):
        self.write({
            'stage': 'progress'
        })

    # This function is triggered when the user clicks on the button 'Done'
    @api.one
    def done_progressbar(self):
        self.write({
            'stage': 'finished',
        })

    # def _default_probability(self):
    #     stage_id = self._default_stage_id()
    #     if stage_id:
    #         return self.env['crm.stage'].browse(stage_id).probability
    #     return 10
    #
    # def _default_stage_id(self):
    #     team = self.env['crm.team'].sudo()._get_default_team_id(user_id=self.env.uid)
    #     return self._stage_find(team_id=team.id, domain=[('fold', '=', False)]).id

    # def _default_stage_id(self):
    #     team = self.env['crm.team'].sudo()._get_default_team_id(user_id=self.env.uid)
    #     return self._stage_find(team_id=team.id, domain=[('fold', '=', False)]).id
    #
    # @api.model
    # def _read_group_stage_ids(self, stages, domain, order):
    #     # retrieve team_id from the context and write the domain
    #     # - ('id', 'in', stages.ids): add columns that should be present
    #     # - OR ('fold', '=', False): add default columns that are not folded
    #     # - OR ('team_ids', '=', team_id), ('fold', '=', False) if team_id: add team columns that are not folded
    #     team_id = self._context.get('default_team_id')
    #     if team_id:
    #         search_domain = ['|', ('id', 'in', stages.ids), '|', ('team_id', '=', False), ('team_id', '=', team_id)]
    #     else:
    #         search_domain = ['|', ('id', 'in', stages.ids), ('team_id', '=', False)]
    #
    #     stage_ids = stages._search(search_domain, order=order, access_rights_uid=SUPERUSER_ID)
    #     return stages.browse(stage_ids)
