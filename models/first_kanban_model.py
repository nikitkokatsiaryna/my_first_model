# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Partner(models.Model):
    # _inherit = 'res.partner'
    _name = 'person'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'utm.mixin', 'format.address.mixin', 'mail.blacklist.mixin']
    # _inherit = ['utm.mixin']

    name = fields.Char()
    last_name = fields.Char()
    birthday = fields.Date()
    age = fields.Integer()
    contact = fields.Char()
    email = fields.Char()
    city = fields.Char()
    phone = fields.Char()

    partner_name = fields.Char()
    street = fields.Char()
    street2 = fields.Char()
    state_id = fields.Integer()
    country_id = fields.Integer()
    website = fields.Char()
    contact_name = fields.Char()
    title = fields.Char()
    function = fields.Char()
    mobile = fields.Integer()


    description = fields.Text()
    # message_bounce = fields.Char()
    checkbox_all = fields.Boolean()
    company_id = fields.Integer()
    campaign_id = fields.Integer()
    medium_id = fields.Integer()
    source_id = fields.Integer()
    referred = fields.Char()
    day_open = fields.Date()
    day_close = fields.Date()
    # probability = fields.Integer()

    checkbox1 = fields.Boolean()
    checkbox2 = fields.Boolean()
    gender = fields.Selection((('male', 'Male'),('female', 'Female'),('other', 'Other')), help='Gender')
