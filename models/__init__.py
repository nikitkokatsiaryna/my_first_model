# -*- coding: utf-8 -*-

from . import models
from . import new_contact_model
from . import module_communication
from . import default_data
from . import first_kanban_model
